import { ResultSetHeader, RowDataPacket } from 'mysql2';
import { connection } from '../db/sql.connection.js';
import { DatabaseException } from '../exceptions/db.exception.js';
import { IIndividualAccountDB } from '../models/account.db.model.js';
import { IIndividualAccount } from '../models/account.model.js';
import IndividualConverter from '../parsers/individual.parser.js';

class IndividualRepository {
  async createIndividualDB(individual: IIndividualAccountDB): Promise<number> {
    const sql = 'INSERT INTO individual_account SET ?';
    try {
      const rows = await connection.query(sql, individual);
      const inserted_individual_account_id: number = Number((rows[0] as ResultSetHeader).insertId);
      return inserted_individual_account_id;
    } catch (err) {
      throw new DatabaseException(err);
    }
  }

  async getIndividualsByIdDB(individuals_id: number[]): Promise<IIndividualAccount[]> {
    if (!Array.isArray(individuals_id) || individuals_id.length === 0) {
      return [];
    }
    const sql =
      `SELECT * FROM account as ai ` +
      'INNER JOIN individual_account as i ON i.account_info_id=ai.account_id ' +
      'LEFT JOIN address as ad ON ad.address_id=i.address_id ' +
      `WHERE ai.account_id IN (${'?' + ',?'.repeat(individuals_id.length - 1)})`;

    try {
      const [rows] = await connection.query(sql, individuals_id);
      return IndividualConverter.rowDataPacketToModel(rows as RowDataPacket[]);
    } catch (err) {
      throw new DatabaseException(err);
    }
  }

  async getIndividualsByPersonIdOrEmailDB(
    individuals_ids: number[],
    emails: string[] | null,
  ): Promise<IIndividualAccount[]> {
    const sql =
      `SELECT * FROM account as ai ` +
      'INNER JOIN individual_account as i ON i.account_info_id=ai.account_id ' +
      'LEFT JOIN address as ad ON ad.address_id=i.address_id ' +
      `WHERE person_id IN (${'?' + ',?'.repeat(individuals_ids.length - 1)})
              ${emails ? `OR email IN (${'?' + ',?'.repeat(emails.length - 1)})` : ''}`;
    try {
      const [rows] = await connection.query(sql, [individuals_ids, emails]);
      return IndividualConverter.rowDataPacketToModel(rows as RowDataPacket[]);
    } catch (err) {
      throw new DatabaseException(err);
    }
  }
}
export default new IndividualRepository();
