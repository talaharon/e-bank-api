import { RowDataPacket, ResultSetHeader } from 'mysql2';
import { connection } from '../db/sql.connection.js';
import { DatabaseException } from '../exceptions/db.exception.js';
import { IAddressDB } from '../models/account.db.model.js';

class AddressRepository {
  async createAdressDB(address: IAddressDB): Promise<number> {
    const sql = 'INSERT INTO address SET ?';
    try {
      const rows = await connection.query(sql, address);
      const inserted_address_id: number = Number((rows[0] as ResultSetHeader).insertId);
      return inserted_address_id;
    } catch (err) {
      throw new DatabaseException(err);
    }
  }

  async getAddressesByIdDB(addresses_id: number[]): Promise<RowDataPacket[]> {
    const sql = `SELECT * FROM address WHERE address_id IN (${
      '?' + ',?'.repeat(addresses_id.length - 1)
    })`;
    try {
      const [rows] = await connection.query(sql, addresses_id);
      return rows as RowDataPacket[];
    } catch (err) {
      throw new DatabaseException(err);
    }
  }
}
export default new AddressRepository();
