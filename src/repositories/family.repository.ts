import { RowDataPacket } from 'mysql2';
import { connection } from '../db/sql.connection.js';
import { DatabaseException } from '../exceptions/db.exception.js';
import { IFamilyAccountDB } from '../models/account.db.model.js';
import { IFamilyAccount, IIndividualAccount } from '../models/account.model.js';
import FamilyConverter from '../parsers/family.parser.js';
import IndividualRepository from './individual.repository.js';
import { FAMILY_ACCOUNT } from './sql.templates.js';

class FamilyRepository {
  async createFamilyDB(family: IFamilyAccountDB): Promise<void> {
    const sql = 'INSERT INTO family_account SET ?';
    try {
      await connection.query(sql, family);
    } catch (err) {
      throw new DatabaseException(err);
    }
  }

  async createFamilyWithOwnersDB(family: IFamilyAccountDB, owners_ids: number[]) {
    try {
      await connection.beginTransaction();
      await this.createFamilyDB(family);
      await this.addIndividualsToFamilyDB(owners_ids, family.account_info_id as number);
      await connection.commit();
    } catch (err) {
      await connection.rollback();
      throw new DatabaseException(err);
    }
  }

  async addIndividualsToFamilyDB(
    individuals_account_id: number[],
    family_account_id: number,
  ): Promise<IIndividualAccount[]> {
    try {
      let family_individual_array: [number, number][] = [];
      for (let id of individuals_account_id) {
        family_individual_array.push([family_account_id, id]);
      }
      const sql = 'INSERT INTO family_individual (family_id,individual_id)' + ' VALUES ?';
      await connection.query(sql, [family_individual_array]);
      const individuals_array: IIndividualAccount[] =
        await IndividualRepository.getIndividualsByIdDB(individuals_account_id);
      return individuals_array;
    } catch (err) {
      throw new DatabaseException(err);
    }
  }

  async getFamilyWithOwnersByIdDB(family_account_id: number): Promise<IFamilyAccount[]> {
    const sql =
      `SELECT ${FAMILY_ACCOUNT} FROM family_account as f ` +
      'LEFT JOIN family_individual as fi ON fi.family_id=f.account_info_id ' +
      'LEFT JOIN individual_account as i ON i.account_info_id=fi.individual_id ' +
      'LEFT JOIN address as ad ON ad.address_id=i.address_id ' +
      'LEFT JOIN account as ai ON ai.account_id=i.account_info_id ' +
      'LEFT JOIN account as af ON af.account_id=f.account_info_id ' +
      'WHERE f.account_info_id=?';
    try {
      const [rows] = await connection.query(sql, family_account_id);
      return FamilyConverter.rowDataPacketToModel(rows as RowDataPacket[]);
    } catch (err) {
      throw new DatabaseException(err);
    }
  }

  async removeIndividualsFromFamilyDB(
    individuals_account_id: number[],
    family_id: number,
  ): Promise<IIndividualAccount[]> {
    try {
      const sql = 'DELETE FROM family_individual WHERE family_id = ? AND individual_id IN (?)';
      await connection.query(sql, [family_id, individuals_account_id]);
      const individuals_array: IIndividualAccount[] =
        await IndividualRepository.getIndividualsByIdDB(individuals_account_id);
      return individuals_array;
    } catch (err) {
      throw new DatabaseException(err);
    }
  }

  async getPrimaryIdsToDeleteIndividualDB(
    family_account_id: number,
    individuals_account_id: number[],
  ): Promise<RowDataPacket[]> {
    const sql =
      'SELECT family_individual_id FROM family_individual ' +
      `WHERE family_id=? AND individual_id IN (${
        '?' + ',?'.repeat(individuals_account_id.length - 1)
      })`;
    try {
      const [rows] = await connection.query(sql, [family_account_id, ...individuals_account_id]);
      return rows as RowDataPacket[];
    } catch (err) {
      throw new DatabaseException(err);
    }
  }
}

export default new FamilyRepository();
