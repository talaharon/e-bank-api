import { RowDataPacket } from 'mysql2';
import { connection } from '../db/sql.connection.js';
import { DatabaseException } from '../exceptions/db.exception.js';
import { IAgent } from '../models/account.model.js';
import AgentConverter from '../parsers/agent.parser.js';

class AgentRepository {
  async getAgentByAccessKey(access_key: string): Promise<IAgent[]> {
    const sql = 'SELECT * FROM agent ' + 'WHERE access_key = ?';
    try {
      const [rows] = await connection.query(sql, access_key);
      return AgentConverter.rowDataPacketToModel(rows as RowDataPacket[]);
    } catch (err) {
      throw new DatabaseException(err);
    }
  }
}

export default new AgentRepository();
