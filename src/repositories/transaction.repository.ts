import { RowDataPacket, ResultSetHeader } from 'mysql2';
import { connection } from '../db/sql.connection.js';
import { DatabaseException } from '../exceptions/db.exception.js';
import { IAccount } from '../models/account.model.js';
import { ITransactionDB, ITransferDB } from '../models/transaction.db.model.js';
import accountRepository from './account.repository.js';

class TransactionRepository {
  private async recordTransactionDB(transaction: ITransactionDB): Promise<RowDataPacket[]> {
    const sql = 'INSERT INTO transaction SET ?';
    try {
      const rows = await connection.query(sql, transaction);
      const inserted_transaction_id: number = Number((rows[0] as ResultSetHeader).insertId);
      return await this.getTransactionsByIdDB([inserted_transaction_id]);
    } catch (err) {
      throw new DatabaseException(err);    }
  }

  private async createTransaction(
    src_id: number,
    dest_id: number,
    src_balance: number,
    dest_balance: number,
  ): Promise<IAccount[]> {
    const sql =
      'UPDATE account SET balance = CASE ' +
      'WHEN account_id = ? THEN ? WHEN account_id = ? THEN ? ' +
      'END WHERE account_id = ? OR account_id = ? ;';
    try {
      await connection.query(sql, [src_id, src_balance, dest_id, dest_balance, src_id, dest_id]);
      const rows = await accountRepository.getAccountsByIdDB([src_id, dest_id]);
      return rows;
    } catch (err) {
      throw new DatabaseException(err);    }
  }

  async createMultiTransfers(transfers: [number, number, number, number][]) {
    try {
      await connection.beginTransaction();
      for (const transfer of transfers) {
        await this.createTransaction(...transfer);
        
      }
      await connection.commit();
    } catch (err) {
      await connection.rollback();
      throw new DatabaseException(err);    }
  }

  async transaction(transfer: ITransferDB): Promise<RowDataPacket[]> {
    try {
      await connection.beginTransaction();
      await this.createTransaction(
        transfer.transaction.src_id,
        transfer.transaction.dest_id,
        transfer.src_balance,
        transfer.dest_balance,
      );
      const accounts_transaction_info = await this.recordTransactionDB(transfer.transaction);
      await connection.commit();
      return accounts_transaction_info;
    } catch (err) {
      await connection.rollback();
      throw new DatabaseException(err);    }
  }

  async getTransactionsByIdDB(transactions_id: number[]): Promise<RowDataPacket[]> {
    const sql = `SELECT * FROM transaction WHERE transaction_id IN (${
      '?' + ',?'.repeat(transactions_id.length - 1)
    })`;
    try {
      const [rows] = await connection.query(sql, transactions_id);
      return rows as RowDataPacket[];
    } catch (err) {
      throw new DatabaseException(err);    }
  }
}
export default new TransactionRepository();
