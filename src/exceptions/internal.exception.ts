import { HttpStatusCode, ResponseStatus } from '../models/enums.js';
import { HttpException } from './http.exception.js';

export class InternalException extends HttpException {
  constructor(public message: string, public payload: unknown) {
    super(ResponseStatus.FAILED, HttpStatusCode.INTERNAL_SERVER_ERROR, message, payload);
  }
}
