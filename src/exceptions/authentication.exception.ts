import { HttpStatusCode, ResponseStatus } from '../models/enums.js';
import { HttpException } from './http.exception.js';

export class AuthenticationException extends HttpException {
  constructor(public message: string, public payload: unknown) {
    super(ResponseStatus.FAILED, HttpStatusCode.NETWORK_AUTHENTICATION_REQUIRED, message, payload);
  }
}
