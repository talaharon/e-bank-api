import { HttpStatusCode, ResponseStatus } from '../models/enums';

export class HttpException extends Error {
  constructor(
    public status: ResponseStatus,
    public status_code: HttpStatusCode,
    public message: string,
    public payload: unknown,
  ) {
    super(message);
  }
}
