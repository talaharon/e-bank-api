import { HttpStatusCode, ResponseStatus } from '../models/enums.js';
import { HttpException } from './http.exception.js';

export class DatabaseException extends HttpException {
  constructor(public payload: unknown) {
    super(ResponseStatus.FAILED, HttpStatusCode.INTERNAL_SERVER_ERROR, "Database error", payload);
  }
}
