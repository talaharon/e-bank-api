import { HttpStatusCode, ResponseStatus } from '../models/enums.js';
import { HttpException } from './http.exception.js';

export class ValidationException extends HttpException {
  constructor(public message: string, public payload: unknown) {
    super(ResponseStatus.FAILED, HttpStatusCode.BAD_REQUEST, message, payload);
  }
}
