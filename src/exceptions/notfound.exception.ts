import { HttpStatusCode, ResponseStatus } from '../models/enums.js';
import { HttpException } from './http.exception.js';

export class NotFoundException extends HttpException {
  constructor(public message: string, public payload: unknown) {
    super(ResponseStatus.FAILED, HttpStatusCode.NOT_FOUND, message, payload);
  }
}
