import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import common_router from '../routes/common.router.js';
import individual_router from '../routes/individual.router.js';
import account_router from '../routes/account.router.js';
import business_router from '../routes/business.router.js';
import family_router from '../routes/family.router.js';

import {
  errorResponse,
  logErrorMiddleware,
  printError,
} from '../middlewares/errors.middlewares.js';
import { httpLogger } from '../middlewares/common.middlewares.js';
import { InputValidationService } from '../modules/validation/validation.service.js';
import { INPUT_VALIDATION_FUNCTIONS } from '../utils/validation.utils.js';
import config from './config.js';

class App {
  public app: express.Application;

  constructor() {
    this.app = express();
    this.configValidation();
    this.configExpressMiddlewares();
    this.configCustomMiddlewares();
    this.configRoutes();
    this.configErrorHandlers();
  }

  configValidation() {
    InputValidationService.configureValidator(INPUT_VALIDATION_FUNCTIONS);
  }

  configErrorHandlers() {
    const { ERR_PATH = './logs/errors.log.txt' } = config.configurations;
    this.app.use(printError);
    this.app.use(logErrorMiddleware(ERR_PATH as string));
    this.app.use(errorResponse);
  }

  private configExpressMiddlewares() {
    this.app.use(cors());
    this.app.use(morgan('dev'));
    this.app.use(express.json());
  }

  private configRoutes() {
    this.app.use('/api/family', family_router);
    this.app.use('/api/business', business_router);
    this.app.use('/api/individual', individual_router);
    this.app.use('/api/account', account_router);
    this.app.use('*', common_router);
  }

  private configCustomMiddlewares() {
    const { LOG_PATH = './logs/http.log.txt' } = config.configurations;
    this.app.use(httpLogger(LOG_PATH as string));
  }
}

export default new App().app;
