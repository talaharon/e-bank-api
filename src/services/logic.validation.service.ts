import { ValidationException } from '../exceptions/validation.exception.js';
import { IAccount, IAgent, IBusinessAccount, IFamilyAccount } from '../models/account.model.js';
import config from '../config/config.js';
const { limited_amount_b2b, limited_amount_b2i, limited_amount_f2b, limited_amount_b2bfx } =
  config.flags;
export class ValidationService {
  // Account
  public static validateActiveDeactiveAccount = (
    accounts: IAccount[],
    requested_accounts_num: number,
    action: string,
  ): boolean => {
    return (
      ValidationService.doEntitiesExist(accounts, requested_accounts_num, true) &&
      ValidationService.checkAccountsTypes(accounts, 'family', false) &&
      ValidationService.isValidActiveDeactive(accounts, action, true)
    );
  };

  // Individual
  public static validateIndividualCreation = (account: IAccount[]): boolean => {
    return !ValidationService.doEntitiesExist(account, 1, false);
  };

  public static validateGetIndividual = (accounts: IAccount[]): boolean => {
    return ValidationService.doEntitiesExist(accounts, 1, true);
  };

  //Business
  public static validateGetBusiness = (accounts: IBusinessAccount[]): boolean => {
    return ValidationService.doEntitiesExist(accounts, 1, true);
  };

  // Family

  public static validateGetFamily = (accounts: IAccount[]): boolean => {
    return ValidationService.doEntitiesExist(accounts, 1, true);
  };

  public static validateFamilyCreation = (
    accounts: IAccount[],
    accounts_tuples: [number, number][],
    expected_currency: string,
  ): boolean => {
    return (
      ValidationService.doEntitiesExist(accounts, accounts_tuples.length, true) &&
      ValidationService.checkAccountsStatuses(accounts, 'active', true) &&
      ValidationService.checkAccountsTypes(accounts, 'individual', true) &&
      ValidationService.checkAccountsCurrencies(accounts, expected_currency, true) &&
      ValidationService.checkIfIndividualsHaveEnoughMoney(
        accounts,
        accounts_tuples.map((tuple: [number, number]) => tuple[1]),
        true,
      )
      //TO ADD VALIDATION FOR MONEY
    );
  };

  public static validateAddMembersToFamily = (
    family_account: IFamilyAccount,
    accounts_to_add: IAccount[],
    accounts_tuples: [number, number][],
  ): boolean => {
    return (
      ValidationService.doesAccountExist(family_account, true) &&
      ValidationService.checkAccountType(family_account, 'family', true) &&
      ValidationService.checkAccountStatus(family_account, 'active', true) &&
      ValidationService.doEntitiesExist(accounts_to_add, accounts_tuples.length, true) &&
      ValidationService.checkAccountsStatuses(accounts_to_add, 'active', true) &&
      ValidationService.checkAccountsTypes(accounts_to_add, 'individual', true) &&
      ValidationService.checkAccountsCurrencies(accounts_to_add, family_account.currency, true) &&
      ValidationService.checkIfIndividualsHaveEnoughMoney(
        accounts_to_add,
        accounts_tuples.map((tuple: [number, number]) => tuple[1]),
        true &&
        ValidationService.areAccountsInFamily(family_account.owners, accounts_to_add, false),
      )
    );
  };

  public static validateRemoveMembersFromFamily = (
    family_account: IFamilyAccount,
    accounts_to_delete: IAccount[],
    accounts_tuples: [number, number][],
  ): boolean => {
    return (
      ValidationService.doesAccountExist(family_account, true) &&
      ValidationService.checkAccountType(family_account, 'family', true) &&
      ValidationService.checkAccountStatus(family_account, 'active', true) &&
      ValidationService.doEntitiesExist(accounts_to_delete, accounts_tuples.length, true) &&
      ValidationService.areAllAccountsInFamily(family_account.owners, accounts_to_delete, true) &&
      ValidationService.checkIfFamilyHasEnoughMoney(
        family_account.balance,
        accounts_tuples.map(tuple => tuple[1]),
        family_account.owners.length,
        true,
      )
    );
  };

  public static validateCloseFamilyAccount = (family_account: IFamilyAccount): boolean => {
    return (
      ValidationService.doesAccountExist(family_account, true) &&
      ValidationService.checkAccountType(family_account, 'family', true) &&
      ValidationService.checkAccountStatus(family_account, 'active', true) &&
      ValidationService.checkIfHasOwners(family_account)
    );
  };

  // Transactions
  public static validateTransferB2B = (src: IAccount, dest: IAccount, amount: number): boolean => {
    return (
      ValidationService.doesAccountExist(src, true) &&
      ValidationService.doesAccountExist(dest, true) &&
      ValidationService.checkAccountStatus(src, 'active', true) &&
      ValidationService.checkAccountStatus(dest, 'active', true) &&
      ValidationService.checkAccountType(src, 'business', true) &&
      ValidationService.checkAccountType(dest, 'business', true) &&
      ValidationService.checkAccountCurrency(dest, src.currency, true) &&
      (!limited_amount_b2b ||
        ValidationService.checkTransferLogic('B2B', src, dest, amount, true)) &&
      src.balance - amount >= 0
    );
  };

  public static validateTransferB2BFX = (
    src: IAccount,
    dest: IAccount,
    amount: number,
  ): boolean => {
    return (
      ValidationService.doesAccountExist(src, true) &&
      ValidationService.doesAccountExist(dest, true) &&
      ValidationService.checkAccountStatus(src, 'active', true) &&
      ValidationService.checkAccountStatus(dest, 'active', true) &&
      ValidationService.checkAccountType(src, 'business', true) &&
      ValidationService.checkAccountType(dest, 'business', true) &&
      ValidationService.checkAccountCurrency(dest, src.currency, false) &&
      (!limited_amount_b2bfx ||
        ValidationService.checkTransferLogic('B2B', src, dest, amount, true)) &&
      src.balance - amount >= 0
    );
  };

  public static validateTransferB2I = (src: IAccount, dest: IAccount, amount: number): boolean => {
    return (
      ValidationService.doesAccountExist(src, true) &&
      ValidationService.doesAccountExist(dest, true) &&
      ValidationService.checkAccountStatus(src, 'active', true) &&
      ValidationService.checkAccountStatus(dest, 'active', true) &&
      ValidationService.checkAccountType(src, 'business', true) &&
      ValidationService.checkAccountType(dest, 'individual', true) &&
      ValidationService.checkAccountCurrency(dest, src.currency, true) &&
      (!limited_amount_b2i ||
        ValidationService.checkTransferLogic('B2I', src, dest, amount, true)) &&
      src.balance - amount >= 0
    );
  };

  public static validateTransferF2B = (src: IAccount, dest: IAccount, amount: number): boolean => {
    return (
      ValidationService.doesAccountExist(src, true) &&
      ValidationService.doesAccountExist(dest, true) &&
      ValidationService.checkAccountStatus(src, 'active', true) &&
      ValidationService.checkAccountStatus(dest, 'active', true) &&
      ValidationService.checkAccountType(src, 'family', true) &&
      ValidationService.checkAccountType(dest, 'business', true) &&
      ValidationService.checkAccountsStatuses((src as IFamilyAccount).owners, 'active', true) &&
      ValidationService.checkAccountsTypes((src as IFamilyAccount).owners, 'individual', true) &&
      ValidationService.checkAccountCurrency(dest, src.currency, true) &&
      ValidationService.checkAccountsCurrencies(
        (src as IFamilyAccount).owners,
        src.currency,
        true,
      ) &&
      (!limited_amount_f2b ||
        ValidationService.checkTransferLogic('F2B', src, dest, amount, true)) &&
      src.balance - amount >= 0
    );
  };

  // Agent

  public static validateGetAgent = (agents: IAgent[]): boolean => {
    return ValidationService.doEntitiesExist(agents, 1, true);
  };

  // utils
  private static checkTransferLogic = (
    transaction_type: string,
    src: IAccount,
    dest: IAccount,
    amount: number,
    expected_result: boolean,
  ): boolean => {
    let res: boolean = true;
    switch (transaction_type) {
      case 'B2B': {
        let allowed_amount_to_transfer =
          (src as IBusinessAccount).company_id === (dest as IBusinessAccount).company_id
            ? 10000
            : 1000;
        res = src.balance - amount >= 10000 && amount <= allowed_amount_to_transfer;
        break;
      }
      case 'B2I': {
        res = src.balance - amount >= 10000 && amount <= 1000;
        break;
      }
      case 'F2B': {
        res = src.balance - amount >= 5000 && amount <= 5000;
        break;
      }
    }

    if (res !== expected_result) {
      throw new ValidationException(`Transaction failed: Failed to transfer money.`, {
        src,
        dest,
        amount,
      });
    }
    return true;
  };

  private static doesAccountExist = (account: IAccount, expected_result: boolean): boolean => {
    if (expected_result !== !!account) {
      throw new ValidationException(
        `Account ${expected_result ? 'does not exist' : 'already exists'}`,
        expected_result ? account : null,
      );
    }

    return true;
  };

  private static checkIfHasOwners = (family_account: IFamilyAccount): boolean => {
    if (family_account.owners.length !== 0) {
      throw new ValidationException(`Family has owners`, null);
    }

    return true;
  };

  private static doEntitiesExist = (
    entities: IAccount[] | IAgent[],
    expected_entity_num: number,
    expected_result: boolean,
  ): boolean => {
    if (expected_result !== (entities.length === expected_entity_num)) {
      throw new ValidationException(
        `Entities ${expected_result ? 'do not exist' : 'already exist'}`,
        expected_result ? { entities, expected_entity_num } : null,
      );
    }
    return true;
  };

  private static checkAccountsStatuses = (
    accounts: IAccount[],
    expected_status: string,
    expected_result: boolean,
  ): boolean => {
    const res = accounts.every(account =>
      ValidationService.checkAccountStatus(account, expected_status, true),
    );
    return res === expected_result;
  };

  private static checkAccountStatus = (
    account: IAccount,
    expected_status: string,
    expected_result: boolean,
  ): boolean => {
    if (expected_result === (account.status !== expected_status)) {
      throw new ValidationException(
        `Status ${expected_result ? 'does not match ' : 'matches'} ${expected_status}`,
        { account, expected_status },
      );
    }
    return account.status === expected_status;
  };

  private static checkAccountsTypes = (
    accounts: IAccount[],
    expected_type: string,
    expected_result: boolean,
  ): boolean => {
    return accounts.every(account =>
      ValidationService.checkAccountType(account, expected_type, expected_result),
    );
  };

  private static checkAccountType = (
    account: IAccount,
    expected_type: string,
    expected_result: boolean,
  ): boolean => {
    if (expected_result != (account.type === expected_type)) {
      throw new ValidationException(
        `Type ${expected_result ? 'does not match ' : 'matches'} ${expected_type}`,
        { account, expected_type },
      );
    }
    return true;
  };

  private static checkAccountsCurrencies = (
    accounts: IAccount[],
    expected_currency: string,
    expected_result: boolean,
  ): boolean => {
    const res = accounts.every(account =>
      ValidationService.checkAccountCurrency(account, expected_currency, true),
    );
    if (res !== expected_result) {
      throw new ValidationException(
        `Currency ${expected_result ? 'does not match ' : 'matches'} ${expected_currency}`,
        { accounts, expected_currency },
      );
    }
    return true;
  };

  private static checkAccountCurrency = (
    account: IAccount,
    expected_currency: string,
    expected_result: boolean,
  ): boolean => {
    if ((account.currency === expected_currency) !== expected_result) {
      throw new ValidationException(
        `Currency ${expected_result ? 'does not match ' : 'matches'} ${expected_currency}`,
        { account, expected_currency },
      );
    }
    return true;
  };

  private static checkIfIndividualsHaveEnoughMoney = (
    accounts: IAccount[],
    amounts_to_transfer: number[],
    expected_result: boolean,
  ): boolean => {
    const res = accounts.every(
      (account, index) => account.balance - amounts_to_transfer[index] > 1000,
    );
    if (res !== expected_result) {
      throw new ValidationException("Individual doesn't have enough money", {
        accounts,
        amounts_to_transfer,
      });
    }
    return true;
  };

  private static checkIfFamilyHasEnoughMoney = (
    family_account_balance: number,
    amounts_to_transfer: number[],
    number_of_family_members:number,
    expected_result: boolean,
  ): boolean => {
    for (const [index, amount] of amounts_to_transfer.entries()) {
      if (index === amounts_to_transfer.length - 1) {
        if (
          expected_result !==
          (((family_account_balance - amount) >= 0 && (number_of_family_members === amounts_to_transfer.length))|| (family_account_balance - amount) >=5000)
        ) {
          throw new ValidationException("Family doesn't have enough money", {
            family_account_balance,
            amounts_to_transfer,
          });
        }
      } else {
        if (expected_result === family_account_balance - amount < 5000) {
          throw new ValidationException("Family doesn't have enough money", {
            family_account_balance,
            amounts_to_transfer,
          });
        }
        family_account_balance -= amount;
      }
    }
    return true;
  };

  private static areAccountsInFamily = (
    family_accounts: IAccount[],
    accounts: IAccount[],
    expected_result: boolean,
  ): boolean => {
    const family_accounts_ids = family_accounts.map(account => account.account_id);
    const accounts_ids = accounts.map(account => account.account_id);
    const res = accounts_ids.some(account_id => family_accounts_ids.includes(account_id));
    if (res !== expected_result) {
      throw new ValidationException('Some accounts are already in family', {
        family_accounts,
        accounts,
      });
    }
    return true;
  };

  private static areAllAccountsInFamily = (
    family_accounts: IAccount[],
    accounts: IAccount[],
    expected_result: boolean,
  ): boolean => {
    const family_accounts_ids = family_accounts.map(account => account.account_id);
    const accounts_ids = accounts.map(account => account.account_id);
    const res = accounts_ids.every(account_id => family_accounts_ids.includes(account_id));
    if (res !== expected_result) {
      throw new ValidationException('Not all accounts are in family', {
        family_accounts,
        accounts,
      });
    }
    return true;
  };

  private static isValidActiveDeactive = (
    accounts: IAccount[],
    action: string,
    expected_result: boolean,
  ): boolean => {
    let should_throw_err: boolean =
      accounts.every(account => account.status === 'active') !== expected_result;
    if (action === 'activate') {
      should_throw_err =
        accounts.every(account => account.status === 'inactive') !== expected_result;
    }
    if (should_throw_err) {
      throw new ValidationException('Not all accounts have the correct status', null);
    }
    return true;
  };
}
