import { IAccount, IFamilyAccount } from '../models/account.model.js';
import { AccountStatus, Entities } from '../models/enums.js';
import { ICreateFamilyAccountDTO } from '../models/requests.dto.model.js';
import FamilyRepository from '../repositories/family.repository.js';
import account_service from './account.service.js';
import { ValidationService } from './logic.validation.service.js';
import FamilyConverter from '../parsers/family.parser.js';
import AccountRepository from '../repositories/account.repository.js';
import AccountService from '../services/account.service.js';
import TransactionRepository from '../repositories/transaction.repository.js';

class FamilyService {
  private getPaymentsToFamily(
    family_id: number,
    family_balance: number,
    owner_tuples: [number, number][],
    owner_accounts: IAccount[],
  ): [number, number, number, number][] {
    let balance = family_balance;
    const payments: [number, number, number, number][] = owner_tuples.map(
      (owner_tuple: [number, number], index) => {
        const [owner_id, owner_amount] = owner_tuple;
        return [
          owner_id,
          family_id,
          owner_accounts[index].balance - owner_amount,
          (balance += owner_amount),
        ];
      },
    );

    return payments;
  }

  private getPaymentsFromFamily(
    family_id: number,
    family_balance: number,
    owner_tuples: [number, number][],
    owner_accounts: IAccount[],
  ): [number, number, number, number][] {
    let balance = family_balance;
    const payments: [number, number, number, number][] = owner_tuples.map(
      (owner_tuple: [number, number], index) => {
        const [owner_id, owner_amount] = owner_tuple;
        return [
          family_id,
          owner_id,
          (balance -= owner_amount),
          owner_accounts[index].balance + owner_amount,
        ];
      },
    );

    return payments;
  }

  async getByID(family_id: number): Promise<IFamilyAccount | null> {
    const family_accounts = await FamilyRepository.getFamilyWithOwnersByIdDB(family_id);
    ValidationService.validateGetFamily(family_accounts);
    return family_accounts[0];
  }

  async closeFamily(family_id: number): Promise<IFamilyAccount> {
    const family_accounts: IFamilyAccount[] = await FamilyRepository.getFamilyWithOwnersByIdDB(
      family_id,
    );
    const family_to_close = family_accounts[0];
    ValidationService.validateCloseFamilyAccount(family_to_close);

    await AccountRepository.updateStatusesAccountsByIdDB(
      [family_to_close.account_id as number],
      AccountStatus.INACTIVE,
    );
    family_to_close.status = AccountStatus.INACTIVE;
    return family_to_close;
  }

  async create(family_DTO: ICreateFamilyAccountDTO): Promise<IFamilyAccount> {
    const owners_ids = family_DTO.owner_tuples.map(tuple_owner => tuple_owner[0]);
    const owner_accounts: IAccount[] = await account_service.getAccountsByID(owners_ids);
    ValidationService.validateFamilyCreation(
      owner_accounts,
      family_DTO.owner_tuples,
      family_DTO.currency,
    );

    // Prepare models
    const [account_db_model, family_db_model] = FamilyConverter.familyDtoToDbModel(family_DTO);

    account_db_model.type = Entities.FAMILY;
    account_db_model.status = AccountStatus.ACTIVE;
    account_db_model.balance = 0;
    // Create account
    const inserted_account_id = await AccountRepository.createAccountDB(account_db_model);

    family_db_model.account_info_id = inserted_account_id;
    // Create family account
    await FamilyRepository.createFamilyWithOwnersDB(family_db_model, owners_ids);

    const payments_in = this.getPaymentsToFamily(
      inserted_account_id,
      0,
      family_DTO.owner_tuples,
      owner_accounts,
    );

    await TransactionRepository.createMultiTransfers(payments_in);

    // CAN BE REPLACED WITH MERGING DBMODELS AND ADDING ACCOUNT ID AND ADDRESS ID
    const family_to_return = (
      await FamilyRepository.getFamilyWithOwnersByIdDB(inserted_account_id)
    )[0];

    return family_to_return;
  }

  async addMembers(family_id: number, owner_tuples: [number, number][]): Promise<IFamilyAccount> {
    const owners_ids = owner_tuples.map(tuple => tuple[0]);
    const family_accounts = await FamilyRepository.getFamilyWithOwnersByIdDB(family_id);
    const accounts_to_add = await AccountService.getAccountsByID(owners_ids);
    ValidationService.validateAddMembersToFamily(family_accounts[0], accounts_to_add, owner_tuples);
    await FamilyRepository.addIndividualsToFamilyDB(owners_ids, family_id);

    const payments_in = this.getPaymentsToFamily(
      family_id,
      family_accounts[0].balance,
      owner_tuples,
      accounts_to_add,
    );
    await TransactionRepository.createMultiTransfers(payments_in);

    const family_to_return = await this.getByID(family_id);
    return family_to_return as IFamilyAccount;
  }

  async removeMembers(
    family_id: number,
    owner_tuples: [number, number][],
  ): Promise<IFamilyAccount> {
    const owners_ids = owner_tuples.map(tuple => tuple[0]);
    const family_accounts = await FamilyRepository.getFamilyWithOwnersByIdDB(family_id);
    const accounts_to_remove = await AccountService.getAccountsByID(owners_ids);
    ValidationService.validateRemoveMembersFromFamily(
      family_accounts[0],
      accounts_to_remove,
      owner_tuples,
    );
    await FamilyRepository.removeIndividualsFromFamilyDB(owners_ids, family_id);
    const payments_out = this.getPaymentsFromFamily(
      family_id,
      family_accounts[0].balance,
      owner_tuples,
      accounts_to_remove,
    );
    await TransactionRepository.createMultiTransfers(payments_out);
    const family_to_return = await this.getByID(family_id);

    return family_to_return as IFamilyAccount;
  }
}

const family_service = new FamilyService();

export default family_service;
