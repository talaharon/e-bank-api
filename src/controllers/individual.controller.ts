import { RequestHandler } from 'express';
import { IIndividualAccount } from '../models/account.model.js';
import { HttpStatusCode, ResponseStatus } from '../models/enums.js';
import individual_service from '../services/individual.service.js';

class IndividualController {
  createIndividual: RequestHandler = async (req, res) => {
    const individual_to_create: IIndividualAccount = req.body;
    const response_data = await individual_service.create(individual_to_create);
    res.status(HttpStatusCode.CREATED).json({
      status: ResponseStatus.SUCCESS,
      code: HttpStatusCode.CREATED,
      message: 'Individual account was created successfully',
      data: response_data,
    });
  };

  getIndividual: RequestHandler = async (req, res) => {
    const individual_id: number = Number(req.params.id);
    const response_data = await individual_service.getIndividualByID(individual_id);
    res.status(HttpStatusCode.OK).json({
      status: ResponseStatus.SUCCESS,
      code: HttpStatusCode.OK,
      message: 'Individual account found',
      data: response_data,
    });
  };
}

export default new IndividualController();
