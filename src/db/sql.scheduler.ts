import child_process from 'child_process';
import { CronJob } from 'cron';
import http from 'http';
import log from '@ajar/marker';

class SqlScheduler {
  runner = (): void => {
    const { DATABASES = 'error extracting backupDBs' } = process.env;
    const parsedDbs = DATABASES.split(',');
    this.helper(parsedDbs);
  };

  helper = (databases: string[]) => {
    if (databases.length === 0) {
      return;
    }

    const spawn = child_process.spawn;

    const dump = spawn('mysqldump', [
      '--databases',
      databases[0],
      '--user',
      'root',
      '--password=qwerty123',
    ]);

    const options = {
      hostname: 'localhost',
      port: 8080,
      method: 'POST',
      path: `/${databases[0]}`,
    };

    const req = http.request(options, res => {
      log.green(`statusCode: ${res.statusCode as number}`);
      log.blue('Backup was done');
    });

    dump.stdout
      .pipe(req)
      .on('close', () => {
        databases.shift();
        this.helper(databases);
      })
      .on('error', function (err: Error) {
        console.log(err);
      });
  };
}

const scheduler = new SqlScheduler();

const job = new CronJob('5 * * * * *', scheduler.runner, null, true, 'Israel');
export default job;
