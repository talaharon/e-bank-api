import { NextFunction, Request, RequestHandler, Response } from 'express';
import AgentService from '../services/agent.service.js';
import crypto from 'crypto';
import { IAgent } from '../models/account.model.js';
import { AuthenticationException } from '../exceptions/authentication.exception.js';

export const verifyAuth: RequestHandler = async (
  req: Request,
  res: Response,
  next: NextFunction,
): Promise<void> => {
  const { method: http_method } = req;
  const {
    'access-key': access_key,
    salt,
    timestamp,
    signature,
  } = req.headers as { 'access-key': string; salt: string; timestamp: string; signature: string };
  if (!access_key || !salt || !timestamp || !signature) {
    next(
      new AuthenticationException('Please provide all the relevent information', {
        access_key,
        salt,
        timestamp,
        signature,
      }),
    );
  }
  let agent;
  try {
    agent = await AgentService.getAgentByAccessKey(access_key);
  } catch (err) {
    next(new AuthenticationException('Auth Error: No such user exists', null));
  }

  const secret_key = (agent as IAgent).secret_key;

  if (!secret_key) {
    next(new AuthenticationException('Auth Error: No such user exists', null));
  }

  let body: string = '';
  if (JSON.stringify(req.body) !== '{}' && req.body !== '') {
    body = JSON.stringify(req.body);
  }
  const to_sign = http_method + salt + timestamp + access_key + secret_key + body;
  let generated_signature = crypto.createHmac('sha256', secret_key).update(to_sign).digest('hex');
  if (generated_signature === signature) {
    next();
  } else {
    next(new AuthenticationException('Auth Error: Signatures do not match', signature));
  }
};
