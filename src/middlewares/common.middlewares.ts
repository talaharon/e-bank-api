import fs from 'fs';
import { RequestHandler } from 'express';
import { HttpException } from '../exceptions/http.exception.js';
import { InternalException } from '../exceptions/internal.exception.js';

export const httpLogger = (path: string): RequestHandler => {
  const httpFile = fs.createWriteStream(path, { flags: 'a' });
  return (req, res, next): void => {
    httpFile.write(`${path} [${Date.now()}] ${req.method} ${req.path} \n`);
    next();
  };
};

export const raw = (func: RequestHandler): RequestHandler => {
  return async function (req, res, next) {
    try {
      // eslint-disable-next-line @typescript-eslint/await-thenable
      await func(req, res, next);
    } catch (err) {
      if (err instanceof HttpException) {
        next(err);
      } else {
        throw new InternalException('Unknown internal exception', err);
      }
    }
  };
};
