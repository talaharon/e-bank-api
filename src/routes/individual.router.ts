import express from 'express';
import individualcontroller from '../controllers/individual.controller.js';
import { verifyAuth } from '../middlewares/auth.middlewares.js';
import { raw } from '../middlewares/common.middlewares.js';
import { inputValidationMiddleware } from '../middlewares/validation.middlewares.js';
import { Entities, ValidationRoutes } from '../models/enums.js';

const individual_router = express.Router();

const entityValidator = inputValidationMiddleware(Entities.INDIVIDUAL);

individual_router.post(
  '/',
  verifyAuth,
  entityValidator(ValidationRoutes.CREATE),
  raw(individualcontroller.createIndividual),
);

individual_router.get(
  '/:id',
  verifyAuth,
  entityValidator(ValidationRoutes.GET),
  raw(individualcontroller.getIndividual),
);

export default individual_router;
