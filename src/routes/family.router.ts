import express from 'express';
import FamilyController from '../controllers/family.controller.js';
import { verifyAuth } from '../middlewares/auth.middlewares.js';
import { raw } from '../middlewares/common.middlewares.js';
import { inputValidationMiddleware } from '../middlewares/validation.middlewares.js';
import { Entities, ValidationRoutes } from '../models/enums.js';

const family_router = express.Router();

const entityValidator = inputValidationMiddleware(Entities.FAMILY);

family_router.post(
  '/',
  verifyAuth,
  entityValidator(ValidationRoutes.CREATE),
  raw(FamilyController.createFamily),
);

family_router.get(
  '/:id',
  verifyAuth,
  entityValidator(ValidationRoutes.GET),
  raw(FamilyController.getFamily),
);

family_router.delete(
  '/remove',
  verifyAuth,
  entityValidator(ValidationRoutes.REMOVE),
  raw(FamilyController.removeMembers),
);

family_router.delete(
  '/close/:id',
  verifyAuth,
  entityValidator(ValidationRoutes.CLOSE),
  raw(FamilyController.closeFamily),
);

family_router.post(
  '/add',
  verifyAuth,
  entityValidator(ValidationRoutes.ADD),
  raw(FamilyController.addMembers),
);

export default family_router;
