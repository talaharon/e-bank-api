import express from 'express';
import { urlNotFound } from '../middlewares/errors.middlewares.js';
//import accountValidators from '../validations/validation.service.js';

const common_router = express.Router();

//when no routes were matched...
common_router.use('*', urlNotFound);

export default common_router;
