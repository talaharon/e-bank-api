import express from 'express';
import AccountController from '../controllers/account.controller.js';
import { verifyAuth } from '../middlewares/auth.middlewares.js';
import { inputValidationMiddleware } from '../middlewares/validation.middlewares.js';
import { Entities, ValidationRoutes } from '../models/enums.js';

import { raw } from '../middlewares/common.middlewares.js';

const account_router = express.Router();
const entityValidator = inputValidationMiddleware(Entities.ACCOUNT);

account_router.put(
  '/status',
  verifyAuth,
  entityValidator(ValidationRoutes.CHANGE_ACCOUNT_STATUS),
  raw(AccountController.setStatus),
);

account_router.post(
  '/b2b',
  verifyAuth,
  entityValidator(ValidationRoutes.TRANSFER),
  raw(AccountController.createTransferB2B),
);

account_router.post(
  '/b2bfx',
  verifyAuth,
  entityValidator(ValidationRoutes.TRANSFER),
  raw(AccountController.createTransferB2BFX),
);

account_router.post(
  '/b2i',
  verifyAuth,
  entityValidator(ValidationRoutes.TRANSFER),
  raw(AccountController.createTransferB2I),
);

account_router.post(
  '/f2b',
  verifyAuth,
  entityValidator(ValidationRoutes.TRANSFER),
  raw(AccountController.createTransferF2B),
);

export default account_router;
