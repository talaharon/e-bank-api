export interface IRule {
  [key: string]: any;
  validation_message: string;
}

export interface IValidationItem {
  key: string;
  rules: IRule[];
}

export interface IErrorItem {
  content: string;
  info: 'Validation Error';
}

export interface IValidationFunctions {
  [key: string]: (inputToCheck: any, valueToCheckAgainst: any) => boolean;
}


export interface IGeneralObject {
  [key: string]: any;
}

export type IValidationRoutesConfig = IValidationFunctions[];

// export enum DefaultValidationFunctions {

// }