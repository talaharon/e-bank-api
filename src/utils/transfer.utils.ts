import fetch from 'node-fetch';

export const getRate = async (base_currency: string, target_currency: string): Promise<number> => {
  const base_url = `http://api.exchangeratesapi.io/latest`;
  const url = `${base_url}?base=${base_currency}&symbols=${target_currency}&access_key=45b9cb933be71c52613afc30f35b3bd6`;
  let response = await fetch(url);
  let json: { rates: { [key: string]: any } } = (await response.json()) as any;
  if (!('rates' in json)) throw new Error(JSON.stringify(json));
  else if (json.rates[target_currency]) return json.rates[target_currency] as number;
  else {
    throw new Error(`currency: ${target_currency} doesn't exist in results.`); //app logic error
  }
};
