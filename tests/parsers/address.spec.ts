import {expect} from "chai";
import { RowDataPacket } from "mysql2";
import { restore } from "sinon";
import AddressConverter from "../../src/parsers/address.parser.js"
import {mock_address_db_parser_arr,mock_address_parser_arr} from "../mock.data.js";

describe("address parser", function() {

    afterEach(()=>{
        restore();
    })
    context("rowDataPacketToModel function",()=>{
  
      it("should be a function", () => {
        // eslint-disable-next-line @typescript-eslint/unbound-method
        expect(AddressConverter.rowDataPacketToModel).to.be.instanceOf(Function);
      });
  
      it("should check if parser return IAddress obj from RowDataPacket obj with the given params", () => {
        const row_data_result=AddressConverter.rowDataPacketToModel(mock_address_db_parser_arr as RowDataPacket[]);
        expect(row_data_result).to.eql(mock_address_parser_arr);
    });
})

})
  
  