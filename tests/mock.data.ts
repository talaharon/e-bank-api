import { RowDataPacket } from "mysql2";
import { IBusinessAccountDB,IAddressDB,IAccountDB,IFamilyAccountDB,IIndividualAccountDB } from "../src/models/account.db.model.js"
import { IBusinessAccount,IAddress,IIndividualAccount ,IAccount, IAgent} from "../src/models/account.model.js"

export const ACCOUNT_ID=7;
export const ADDRESS_ID=1;


export const mock_address_db:IAddressDB|IAddress={
    address_id: ADDRESS_ID,
    country_name: "Israel",
    country_code: "il",
    postal_code: 200,
    city: "Tel Aviv",
    region: "la",
    street_name: "hamasger",
    street_number: 22
   }

export const mock_address_parser_arr:IAddressDB[]|IAddress[]=
[mock_address_db,mock_address_db];

   
export const mock_business:IBusinessAccount[]=
[
{
    account_id: ACCOUNT_ID,
    currency: "EUR",
    balance: 1000,
    status: 'active',
    type: "business",
    agent_id: 1,
    company_id: 12345678,
    company_name: "rapyd",
    context: "transfers",
    address: mock_address_db,
    
    }

]

export const mock_business_db_parser_arr:IBusinessAccount[]=[mock_business[0]];


export const mock_business_db:IBusinessAccountDB={
   
    business_id: 1,
    company_id: 12345678,
    company_name: "rapyd",
    context: "transfers",
    address_id: ADDRESS_ID,
    account_info_id: ACCOUNT_ID
}

export const mock_account:IAccountDB|IAccount={
  account_id: ACCOUNT_ID,
  currency: "EUR",
  balance: 10000,
  status: 'active',
  type: 'business',
  agent_id:1
}


export const mock_individual_db:IIndividualAccountDB={
    individual_id: 1,
    person_id: 318298800,
    first_name: "Hadar",
    last_name: "Liberman",
    address_id: ADDRESS_ID,
    email: "hadar@gmail.com",
    account_info_id: ACCOUNT_ID,
}



export const mock_individual:IIndividualAccount[]=
[
{
    
  individual_id: 1,
  first_name: "Hadar",
  last_name: "Liberman",
  address:mock_address_db,
  email:"hadar@gmail.com",
  account_id: ACCOUNT_ID,
  currency: "EUR",
  balance: 10000,
  status: 'active',
  type: 'individual',
  agent_id:1

}
]
export const mock_account_parser:IAccount |IAccountDB= 
{
    account_id:ACCOUNT_ID,
    currency:"EUR",
    balance:2000,
    status:'active',
    type:'individual',
    agent_id:1,
    
}

export const mock_account_db_parser_arr:any[]=[mock_account_parser,mock_account_parser]


export const mock_account_parser_arr:IAccount[] |IAccountDB[]= 
[
{
    account_id:ACCOUNT_ID,
    currency:"EUR",
    balance:2000,
    status:'active',
    type:'individual',
    agent_id:1,
    
},
{
    
    account_id:ACCOUNT_ID,
    currency:"EUR",
    balance:2000,
    status:'active',
    type:'individual',
    agent_id:1,
    
},
]

export const mock_address_db_parser_arr:any[]=[mock_address_db,mock_address_db];

export const mock_agent_parser:IAgent={
    agent_id: 1,
    access_key: "access key",
    secret_key: "secret key"
}
export const mock_agent_parser_arr:IAgent[]=[mock_agent_parser,mock_agent_parser];

export const mock_agent_db_parser_arr:any[]=[mock_agent_parser,mock_agent_parser];
// export const mock_address_parser_arr:IAddress[]|IAccountDB[]= 
// [
//     {
//         address_id: ADDRESS_ID,
//         country_name: "Israel",
//         country_code: "il",
//         postal_code: 200,
//         city: "Tel Aviv",
//         region: "la",
//         street_name: "hamasger",
//         street_number: 22
//        },
//        {
//         address_id: ADDRESS_ID,
//         country_name: "Israel",
//         country_code: "il",
//         postal_code: 200,
//         city: "Tel Aviv",
//         region: "la",
//         street_name: "hamasger",
//         street_number: 22
//        },
// ]



  

