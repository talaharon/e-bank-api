/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { expect } from "chai";
import sinon, { restore } from "sinon";
import businessService from "../../src/services/business.service.js";
import addressRepository from "../../src/repositories/address.repository.js";
import accountRepository from "../../src/repositories/account.repository.js";
import {ValidationService} from '../../src/services/logic.validation.service.js';
import { HttpException } from "../../src/exceptions/http.exception.js";
import IndividualRepository from "../../src/repositories/individual.repository.js";
import { ADDRESS_ID, ACCOUNT_ID, mock_account, mock_address_db, mock_individual,mock_individual_db } from "../mock.data.js";
import individual_service from "../../src/services/individual.service.js";
import individual_converter from "../../src/parsers/individual.parser.js";
import { IAccountDB } from "../../src/models/account.db.model.js";
describe("Individual service",  () => {

    // eslint-disable-next-line @typescript-eslint/require-await
    beforeEach(async function () {
        sinon.stub(IndividualRepository, "getIndividualsByIdDB").resolves(mock_individual);
    });

    afterEach(function () {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        (IndividualRepository as any).getIndividualsByIdDB.restore();
        restore();
    });

    context("create function", () => {
        it("should accept and return IIndividualAccount", async () => {
        sinon.stub(IndividualRepository, "getIndividualsByPersonIdOrEmailDB").resolves(mock_individual);
        sinon.stub(ValidationService, "validateIndividualCreation").returns(true);
        sinon.stub(individual_converter, "individualModelToDb").returns([mock_account as IAccountDB, mock_address_db, mock_individual_db]);
        sinon.stub(addressRepository, "createAdressDB").resolves(ADDRESS_ID);
        sinon.stub(accountRepository, "createAccountDB").resolves(ACCOUNT_ID);
        sinon.stub(IndividualRepository, "createIndividualDB").resolves(ACCOUNT_ID);
        const result = await individual_service.create(mock_individual[0]);
            // eslint-disable-next-line @typescript-eslint/unbound-method
        expect(businessService.create).to.be.a("function");
            //@ts-ignore
        expect(result).to.eql(mock_individual[0]);

        })
    });
    context("getIndividualByID function", () => {

        it("success when the Individual found and return IIndividualAccount", async () => {
            sinon.stub(ValidationService, "validateGetIndividual").returns(true);
            const result:any = await individual_service.getIndividualByID(ACCOUNT_ID);
            // eslint-disable-next-line @typescript-eslint/unbound-method
            expect(individual_service.getIndividualByID).to.be.a("function");
            //@ts-ignore
            expect(await result).to.eql(mock_individual[0]);

        });

        it("throw error when the business is not valid", async () => {
            sinon.stub(ValidationService, "validateGetIndividual").returns(false);
            // eslint-disable-next-line @typescript-eslint/unbound-method
            expect(individual_service.getIndividualByID).to.be.a("function");
            try {
                await individual_service.getIndividualByID(ACCOUNT_ID);
            } catch (error) {
                expect((error as HttpException).message).to.be.equal("PLACEHOLDER FOR VALIDATION ERROR");
            }
        });
    })
})
